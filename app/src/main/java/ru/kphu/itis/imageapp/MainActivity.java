package ru.kphu.itis.imageapp;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class MainActivity extends AppCompatActivity {

    private static final String URL = "https://cs8.pikabu.ru/post_img/big/2016/11/29/10/1480439597175676615.jpg";

    private ImageView imageView;

    //private TextView textView;

    //private Typeface typeface;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        imageView = findViewById(R.id.image_view);
        Glide.with(this)
                .load(URL)
                .into(imageView);

        /*typeface = Typeface.createFromAsset(getAssets(), "fonts/DigitalStripCyrillic.ttf");

        textView = findViewById(R.id.text_view);
        textView.setTypeface(typeface);*/

    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
